using UnityEngine;
using UnityEngine.UI;

namespace Logic

{
    public class RotateSelf : MonoBehaviour
    {
        public float rotateSpeed;
        public float currentRotate;

        private void Awake()
        {
            currentRotate = 0;
        }
        private void FixedUpdate()
        {
            currentRotate += Time.fixedDeltaTime * rotateSpeed;
            transform.rotation = Quaternion.Euler(0, 0, currentRotate);
        }

    }
}