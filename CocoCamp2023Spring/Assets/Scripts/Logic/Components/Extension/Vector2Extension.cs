using UnityEngine;

namespace Logic
{
    public static class Vector2Extension
    {
        public static bool ApproachToZero(this Vector2 vector2)
        {
            return vector2.sqrMagnitude < 0.00001f;
        }

        public static bool BiggerThanZero(this Vector2 vector2)
        {
            return vector2.sqrMagnitude >= 0.00001f;
        }
    }
}