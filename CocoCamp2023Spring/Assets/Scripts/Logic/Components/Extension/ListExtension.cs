using System.Collections.Generic;
using UnityEngine;

namespace Logic
{
    public static class ListExtension
    {
        public static List<T> RandomPick<T>(this List<T> @this, int pickCount)
        {
            return StaticUtils.RandomPick(@this, pickCount);
        }

        public static T RandomPick<T>(this List<T> @this)
        {
            return StaticUtils.RandomPick(@this);
        }
    }
}