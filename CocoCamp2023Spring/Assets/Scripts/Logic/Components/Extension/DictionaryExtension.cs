using UnityEngine;
using System.Collections.Generic;

namespace Logic

{
    public static class DictionaryExtension
    {
        public static void SetSafely<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue value, bool replaceExistValue = true)
        {
            if(dictionary.ContainsKey(key))
            {
                if(replaceExistValue)
                {
                    dictionary[key] = value;
                }
            }
            else
            {
                dictionary.Add(key, value);
            }
        }

        public static TValue GetSafely<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key)
        {
            if(dictionary.ContainsKey(key))
            {
                return dictionary[key];
            }
            else
            {
                return default(TValue);
            }
        }
    }
}