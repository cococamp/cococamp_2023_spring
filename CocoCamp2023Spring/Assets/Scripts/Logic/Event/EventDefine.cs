using UnityEngine;
using System;
using Config;
using Input;
using UnityEngine.InputSystem;

namespace Logic
{
    namespace EventDefine
    {
        public class ModulesInitFinished : Event { };
        public class SceneInitFinished : Event { };

        public class InputEvent : Event<int, InputAction.CallbackContext> { };
        public class InputKeyboardEvent : Event<InputAction.CallbackContext> { };

        #region LeaderBoard
        public class LeaderBoardRefreshEvent : Event { }
        public class UploadScoreSuccessEvent : Event<int> { }
        public class NameRefreshEvent : Event { }
        #endregion
    }

    namespace EventParam
    {

    }
}
