using Config;
using UnityEngine;

namespace Logic
{
    public class PlayerModel : BaseModel
    {
        private PlayerData[] playerDatas;
        public override void Init()
        {
            playerDatas = new PlayerData[PlayerController.PlayerCount];
            for (int i = 0; i < PlayerController.PlayerCount; i++)
            {
                playerDatas[i] = new PlayerData();
            }
        }
        public override void Destroy()
        {
            playerDatas = null;
        }

        public void ResetPlayerDatas()
        {
            Init();
        }

        public PlayerData GetPlayerData(int playerIndex)
        {
            if (playerDatas != null && playerIndex < playerDatas.Length)
            {
                return playerDatas[playerIndex];
            }
            return null;
        }

        public PlayerData GetMinScorePlayerData()
        {
            if (playerDatas == null || playerDatas.Length == 0) return null;
            int index = 0;
            int score = playerDatas[index].score;
            for (int i = 1; i < playerDatas.Length; i++)
            {
                if (playerDatas[i].score < score)
                {
                    index = i;
                    score = playerDatas[i].score;
                }
            }
            return playerDatas[index];
        }

        public PlayerData GetMaxScorePlayerData()
        {
            if (playerDatas == null || playerDatas.Length == 0) return null;
            int index = 0;
            int stage = 0;
            for (int i = 0; i < playerDatas.Length; i++)
            {
                if (playerDatas[i].stage > stage)
                {
                    index = i;
                    stage = playerDatas[i].stage;
                }
            }
            return playerDatas[index];
        }

        private float reduceScoreTimer;
        public void Update()
        {
            if (playerDatas != null)
            {
                float reduceOneScoreNeedTime = 1 / ConfigManager.instance.gameConfig.scoreReduceSpeed;
                reduceScoreTimer += Time.fixedDeltaTime;
                while (reduceScoreTimer >= reduceOneScoreNeedTime)
                {
                    reduceScoreTimer -= reduceOneScoreNeedTime;
                    foreach (var data in playerDatas)
                    {
                        if (data.score > data.stageConfig.scoreFiled.x)
                        {
                            data.AddScore(-1);
                        }
                    }
                }
            }
        }
    }

    public class PlayerData
    {
        public int score { get; private set; } = 0;

        public void AddScore(int addScore)
        {
            this.score += addScore;
            this.score = Mathf.Max(this.score, 0);
        }

        public int stage => ConfigManager.instance.gameConfig.GetStageIndexByScore(score);

        public StageConfig stageConfig => ConfigManager.instance.gameConfig.GetStageConfigByScore(score);

        public float inertia => ConfigManager.instance.gameConfig.GetStageConfigByScore(score).inertia;
        public float acc => ConfigManager.instance.playerConfig.GetAccByInertia(inertia);
    }
}