namespace Logic
{
    public class PlayerController : BaseController<PlayerController, PlayerModel>
    {
        public const int PlayerCount = 2;
        private PlayerEntity[] players;

        protected override void OnDestroy()
        {
            DestroyPlayers();
        }


        public void CreatePlayers()
        {
            players = new PlayerEntity[PlayerCount];
            for (int i = 0; i < PlayerCount; i++)
            {
                players[i] = new PlayerEntity();
                players[i].Init(i);
                players[i].Show();
            }
            Model.ResetPlayerDatas();
        }

        public void DestroyPlayers()
        {
            if (players == null) return;
            foreach (var player in players)
            {
                player.Destroy();
            }
            players = null;
        }

        protected override void OnFixedUpdate()
        {
            Model.Update();
            if (players == null) return;
            foreach (var player in players)
            {
                player.Update();
            }
        }

        public PlayerEntity GetPlayer(int playerIndex)
        {
            if (players != null && playerIndex < players.Length)
            {
                return players[playerIndex];
            }
            return null;
        }
    }
}