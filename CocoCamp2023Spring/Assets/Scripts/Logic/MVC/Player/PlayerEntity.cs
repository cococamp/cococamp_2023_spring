using UnityEngine;
using UnityEngine.InputSystem;
using Config;
using System.Collections.Generic;

namespace Logic
{
    public class PlayerEntity : BaseEntity<PlayerEntityComp>
    {
        protected override string prefabName => "Player/player";
        public PlayerData PlayerData => PlayerController.instance.Model.GetPlayerData(playerIndex);
        public int playerIndex;

        public void Init(int playerIndex)
        {
            this.playerIndex = playerIndex;
            base.Init();
        }
        protected override void OnInit()
        {
            sceneComponent.entity = this;
            InitControl();
            //InitDebugUI();
        }

        protected override void RegisterEvents()
        {
            EventManager.Get<EventDefine.InputEvent>().AddListener(OnInput);
        }

        protected override void OnShow()
        {

        }
        protected override void OnHide()
        {

        }

        protected override void UnRegisterEvents()
        {
            EventManager.Get<EventDefine.InputEvent>().RemoveListener(OnInput);
        }
        protected override void OnDestroy()
        {
            sceneComponent.entity = null;
            //DestroyDebugUI();
        }
        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnUpdate()
        {
            UpdatePosition();
            //UpdateDebugUI();
        }

        #region Trigger
        private List<OptionEntity> aboveOptions = new List<OptionEntity>();

        protected override void OnTriggerEnter2D(GameObject obj, Collider2D collider2D)
        {
            var optionEntityComp = collider2D.GetRootGO<OptionEntityComp>();
            if (optionEntityComp != null && optionEntityComp.entity != null)
            {
                aboveOptions.Add(optionEntityComp.entity);
            }
        }

        protected override void OnTriggerExit2D(GameObject obj, Collider2D collider2D)
        {
            var optionEntityComp = collider2D.GetRootGO<OptionEntityComp>();
            if (optionEntityComp != null && optionEntityComp.entity != null)
            {
                aboveOptions.Remove(optionEntityComp.entity);
            }

        }
        #endregion


        #region Control
        public Vector2 acc { get; private set; }
        public Vector2 vel { get; private set; }
        public Vector2 pos { get => new Vector2(sceneObject.transform.position.x, sceneObject.transform.position.y); private set => sceneObject.transform.position = value; }
        private void InitControl()
        {
            pos = SceneController.instance.playAreaEntity.CenterPos;
            acc = Vector2.zero;
            vel = Vector2.zero;
        }
        private void OnInput(int playerIndex, InputAction.CallbackContext context)
        {
            if (playerIndex != this.playerIndex) return;
            if (context.action.name.Contains("Move"))
            {
                OnMove(context);
            }
            else if (context.action.name.Contains("Click") && context.phase == InputActionPhase.Started)
            {
                if (aboveOptions.Count > 0)
                {
                    AudioManager.instance.PlayEffect("click");
                    for (int i = aboveOptions.Count - 1; i >= 0; i--)
                    {
                        aboveOptions[i].Click(playerIndex);
                    }
                }
                else
                {
                    AudioManager.instance.PlayEffect("effect7");
                }
            }
        }

        //加速
        private void OnMove(InputAction.CallbackContext context)
        {
            var moveInput = context.ReadValue<Vector2>();
            acc = PlayerData.acc * moveInput.normalized;
        }

        //减速以及更新位置
        private void UpdatePosition()
        {
            //加速
            if (acc.BiggerThanZero())
            {
                var deltaVel = acc * Time.fixedDeltaTime;
                vel += deltaVel;
            }
            //减速
            else if (vel.BiggerThanZero())
            {
                var deltaVelValue = PlayerData.acc * Time.fixedDeltaTime;
                deltaVelValue = Mathf.Min(deltaVelValue, vel.magnitude);
                var deltaVel = -deltaVelValue * vel.normalized;
                vel += deltaVel;
            }

            //限定速度
            var maxSpeed = ConfigManager.instance.playerConfig.maxVel;
            if (vel.magnitude > maxSpeed)
            {
                vel = vel.normalized * maxSpeed;
            }
            else if (vel.ApproachToZero())
            {
                vel = Vector2.zero;
            }

            //更新位置
            if (vel.BiggerThanZero())
            {
                var newPos = pos + vel;
                if (CheckPlayAreaBorder(newPos, out var boarderPos))
                {
                    //要根据光标的大小来调整
                    //往回拉0.1
                    newPos = boarderPos - vel.normalized * 0.1f;
                    vel = Vector2.zero;
                }

                pos = newPos;
            }
        }

        //检测是否碰到墙了
        public bool CheckPlayAreaBorder(Vector2 targetPos, out Vector2 borderPos)
        {
            //目标点还在地板里面  不需要检测新的位置
            if (Physics2D.OverlapPoint(targetPos, LayerMask.GetMask("PlayArea")) != null)
            {
                borderPos = Vector2.zero;
                return false;
            }

            //由于玩家在碰撞体里面  所以要反着来检测
            var dir = pos - targetPos;
            var hit = Physics2D.Raycast(targetPos, dir, dir.magnitude, LayerMask.GetMask("PlayArea"));
            //Debug.Log($"start:{pos} target:{targetPos} distance:{dir.magnitude}  hitDistance{hit.distance} hitPoint{hit.point}  hitCollider:{hit.collider?.name}", hit.collider.gameObject);
            borderPos = hit.point;
            return hit.distance > 0;
        }
        #endregion

        #region Debug
        private DebugUI debugUI;
        private void InitDebugUI()
        {
            debugUI = DebugController.instance.CreateDebugUI(() =>
            {
                var playerData = PlayerController.instance.Model.GetPlayerData(playerIndex);
                var stageConfig = ConfigManager.instance.gameConfig.stageDivideShow[playerData.stage];
                string[] logs = new string[]
                {
                    $"ID:{playerIndex}",
                    $"Acc:{acc}/{acc.magnitude}",
                    $"Vel:{vel}/{vel.magnitude}",
                    $"Pos:{pos}",
                    $"Score:{playerData.score}/ {ConfigManager.instance.gameConfig.maxScore}",
                    $"Stage:{stageConfig.name}/{playerData.stage}/{ConfigManager.instance.gameConfig.stageCount - 1}",
                    $"OptionCount:{aboveOptions.Count}",
                };
                return string.Join("\n", logs);
            }, new Vector2(playerIndex, 1));
        }

        private void DestroyDebugUI()
        {
            DebugController.instance.DestroyDebugUI(debugUI);
            debugUI = null;
        }

        private void UpdateDebugUI()
        {
            debugUI.Update();
        }

        #endregion
    }
}