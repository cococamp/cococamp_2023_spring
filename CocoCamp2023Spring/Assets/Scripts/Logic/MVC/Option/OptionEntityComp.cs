using UnityEngine;

namespace Logic
{
    public class OptionEntityComp : MonoBehaviour
    {
        public TextMesh text;
        public SpriteRenderer spriteRenderer;

        public Rigidbody2D physicsBody;
        public OptionEntity entity;
        public TextMesh answer;
        public Animator animator;

        private bool _select = false;
        public bool select
        {
            set
            {
                if (value != _select)
                {
                    _select = value;
                    spriteRenderer.color = _select ? new Color(0.8f, 0.8f, 0.8f, 1) : new Color(1, 1, 1, 1);
                }
            }
        }
    }
}