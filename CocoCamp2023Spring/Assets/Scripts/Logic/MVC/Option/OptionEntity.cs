using UnityEngine;
using UnityEngine.InputSystem;
using Config;
using System.Collections.Generic;
using DG.Tweening;

namespace Logic
{
    public class OptionEntity : BaseEntity<OptionEntityComp>
    {
        protected override string prefabName => "Option/option";
        public int optionIndex;
        public OptionItemConfig Config => ConfigManager.instance.optionConfig.itemList[optionIndex];

        public void Init(int optionIndex)
        {
            this.optionIndex = optionIndex;
            base.Init();
        }
        protected override void OnInit()
        {
            sceneComponent.entity = this;
            InitValue();
            InitMove();
            RefreshSelectState();
        }

        protected override void RegisterEvents()
        {

        }

        protected override void OnShow()
        {
            AudioManager.instance.PlayEffect("option_show");
            RefreshView();

            sceneComponent.answer.gameObject.SetActiveOptimize(false);
            sceneComponent.animator.Play("idle");
            sceneComponent.text.gameObject.SetActiveOptimize(true);
        }
        protected override void OnHide()
        {

        }

        protected override void UnRegisterEvents()
        {

        }
        protected override void OnDestroy()
        {
            sceneComponent.entity = null;
        }
        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnUpdate()
        {
            UpdateMove();
            UpdateValue();
        }

        public Vector2 acc { get; private set; }
        public Vector2 pos { get => new Vector2(sceneObject.transform.position.x, sceneObject.transform.position.y); private set => sceneObject.transform.position = value; }
        public float ageTime { get; private set; }
        public int clickCount { get; private set; }
        private Vector4 perlinNoisePos;
        private Vector4 perlinNoiseDir;

        private void InitMove()
        {
            pos = SceneController.instance.playAreaEntity.CenterPos + Random.value * SceneController.instance.playAreaEntity.Radius * new Vector2(Random.value, Random.value).normalized;
            acc = Vector2.zero;

            perlinNoisePos = new Vector4(Random.value, Random.value, Random.value, Random.value);
            var dir1 = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)).normalized;
            var dir2 = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)).normalized;
            perlinNoiseDir = new Vector4(dir1.x, dir1.y, dir2.x, dir2.y);
        }


        private void UpdateMove()
        {
            switch (Config.flyType)
            {
                case OptionFlyType.PerlinNoise:
                    perlinNoisePos += perlinNoiseDir * ConfigManager.instance.optionConfig.commonConfig.perlinShakeHz * Time.fixedDeltaTime;
                    acc = new Vector2(Mathf.PerlinNoise(perlinNoisePos.x, perlinNoisePos.y) * 2 - 1, Mathf.PerlinNoise(perlinNoisePos.z, perlinNoisePos.w) * 2 - 1);
                    acc *= 10;
                    acc *= Config.flySpeed;

                    //回心力
                    float distancePercent = Vector2.Distance(pos, SceneController.instance.playAreaEntity.CenterPos) / SceneController.instance.playAreaEntity.Radius;
                    float startPercent = ConfigManager.instance.optionConfig.commonConfig.perlinCenterGravityStartPercent;
                    if (distancePercent > startPercent)
                    {
                        float gravity = (distancePercent - startPercent) / (1 - startPercent) * ConfigManager.instance.optionConfig.commonConfig.perlinCenterGravityMaxValue;
                        Vector2 dir = (SceneController.instance.playAreaEntity.CenterPos - pos).normalized;
                        acc += dir * gravity;
                    }

                    break;
            }

            sceneComponent.physicsBody.AddForce(acc);
        }

        private void InitValue()
        {
            ageTime = 0;
            clickCount = Config.clickCount;
        }

        private void UpdateValue()
        {
            ageTime += Time.fixedDeltaTime;
            if (ageTime > Config.lifeTime)
            {
                OptionController.instance.DestroyOption(this);
                return;
            }
        }

        public void Click(int playerIndex)
        {
            clickCount--;
            RefreshView();
            if (clickCount <= 0)
            {
                AudioManager.instance.PlayEffect("score2");
                AudioManager.instance.PlayEffect("option_destroy");
                sceneComponent.animator.Play("destroy");
                sceneComponent.answer.gameObject.SetActiveOptimize(true);
                sceneComponent.text.gameObject.SetActiveOptimize(false);
                PlayerController.instance.Model.GetPlayerData(playerIndex)?.AddScore(Config.addScore);
                DOTween.Sequence()
                .InsertCallback(1.0f, () =>
                {
                    OptionController.instance.DestroyOption(this);
                });

                return;
            }
        }

        private void RefreshView()
        {
            sceneComponent.text.text = clickCount.ToString();
            sceneComponent.answer.text = Config.answer1;
            RefreshSelectState();
        }

        #region Trigger
        public HashSet<PlayerEntity> abovePlayerEntities = new HashSet<PlayerEntity>();
        protected override void OnTriggerEnter2D(GameObject obj, Collider2D collider2D)
        {
            var playerEntity = collider2D.GetRootGO<PlayerEntityComp>()?.entity;
            if (playerEntity != null)
            {
                AudioManager.instance.PlayEffect("enter_option");
                abovePlayerEntities.Add(playerEntity);
                RefreshSelectState();
            }
        }

        protected override void OnTriggerExit2D(GameObject obj, Collider2D collider2D)
        {
            var playerEntity = collider2D.GetRootGO<PlayerEntityComp>()?.entity;
            if (playerEntity != null)
            {
                AudioManager.instance.PlayEffect("exit_option");
                abovePlayerEntities.Remove(playerEntity);
                RefreshSelectState();
            }
        }

        private void RefreshSelectState()
        {
            sceneComponent.select = abovePlayerEntities.Count > 0;
        }

        #endregion
    }
}