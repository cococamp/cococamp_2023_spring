using System.Collections.Generic;
using UnityEngine;
using Config;
namespace Logic
{
    public class OptionController : BaseController<OptionController, OptionModel>
    {
        private List<OptionEntity> options;
        public IEnumerable<OptionEntity> Options => options;
        protected override void OnInit()
        {
            options = new List<OptionEntity>();
            //InitDebugUI();
        }
        protected override void RegisterEvents()
        {

        }

        protected override void UnRegisterEvents()
        {

        }

        protected override void RegisterAllUI()
        {

        }

        protected override void OnDestroy()
        {
            //DestroyDebugUI();
        }


        protected override void OnFixedUpdate()
        {
            UpdateCreateOptionTimer();
            UpdateAllOptions();
            //UpdateDebugUI();
        }

        public void CreateOption(int optionIndex)
        {
            var newOption = new OptionEntity();
            newOption.Init(optionIndex);
            newOption.Show();
            options.Add(newOption);
        }

        private void UpdateAllOptions()
        {
            if (options == null) return;
            for (int i = options.Count - 1; i >= 0; i--)
            {
                options[i].Update();
            }
        }

        public void DestroyOption(OptionEntity optionEntity)
        {
            options.Remove(optionEntity);
            optionEntity.Destroy();
        }

        public void DestroyAllOption()
        {
            foreach (var option in options)
            {
                option.Destroy();
            }
            options.Clear();
        }

        #region CreateTimer
        private float createTimer;
        private bool createTimerStart;

        public void StartCreateTimer()
        {
            createTimerStart = true;
            createTimer = 0;
            CreateGroupOption();
        }
        private void UpdateCreateOptionTimer()
        {
            if (!createTimerStart) return;
            createTimer += Time.fixedDeltaTime;
            var interval = PlayerController.instance.Model.GetMinScorePlayerData().stageConfig.createOptionInterval;
            if (createTimer > interval)
            {
                createTimer = 0;

                CreateGroupOption();
            }
        }

        private void CreateGroupOption()
        {
            var stage = PlayerController.instance.Model.GetMinScorePlayerData().stage;
            foreach (var index in ConfigManager.instance.optionConfig.GetRandomOptionGroupByStage(stage).list)
            {
                CreateOption(index);
            }
        }

        public void StopCreateTimer()
        {
            createTimerStart = false;
        }

        #endregion

        #region Debug
        private DebugUI debugUI;
        private void InitDebugUI()
        {
            debugUI = DebugController.instance.CreateDebugUI(() =>
            {
                string log = $"option count:{options.Count}";
                foreach (var option in options)
                {
                    string question = option.Config.question.IsFilled() ? (option.Config.question.Length > 5 ? option.Config.question.Substring(0, 5) : option.Config.question) : "Empty";
                    log += $"\n{question}";
                    log += $"\n  acc:{option.acc}/{option.acc.magnitude}";
                    log += $"\n  vel:{option.sceneComponent.physicsBody.velocity}/{option.sceneComponent.physicsBody.velocity.magnitude}";
                    log += $"\n  pos:{option.pos}";
                    log += $"\n  life:{option.ageTime}/{option.Config.lifeTime}";
                    log += $"\n  click:{option.clickCount}/{option.Config.clickCount}";
                    log += $"\n  playerCount:{option.abovePlayerEntities.Count}";
                }
                return log;
            }, new Vector2(1, 0));
        }

        private void DestroyDebugUI()
        {
            DebugController.instance.DestroyDebugUI(debugUI);
            debugUI = null;
        }

        private void UpdateDebugUI()
        {
            debugUI.Update();
        }
        #endregion
    }
}