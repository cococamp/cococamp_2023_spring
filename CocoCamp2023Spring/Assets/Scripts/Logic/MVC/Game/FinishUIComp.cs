using UnityEngine;
using UnityEngine.UI;

namespace Logic
{
    public class FinishUIComp:MonoBehaviour
    {
        public Text text;
        public GameObject txtBg;
        public Image bg;
        public Sprite[] bgs;
        public Animator animator;
    }
}