using UnityEngine;

namespace Logic
{
    public class GameModel : BaseModel
    {
        public float gameTime { get; private set; }
        private bool isStart;
        public override void Init()
        {
            gameTime = 0;
            isStart = false;
        }

        public void FixedUpdate()
        {
            if (isStart)
            {
                gameTime += Time.fixedDeltaTime;
            }
        }

        public void StartGameTime()
        {
            isStart = true;
            gameTime = 0;
        }

        public void StopGameTime()
        {
            isStart = false;
        }


        public override void Destroy()
        {

        }
    }
}