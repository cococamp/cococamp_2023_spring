using UnityEngine;
using UnityEngine.UI;

namespace Logic
{
    public class MainUIComp:MonoBehaviour
    {
        public Text tips;
        public RectTransform player0Value;
        public RectTransform player1Value;
        public Text player0Text;
        public Text player1Text;
    }
}