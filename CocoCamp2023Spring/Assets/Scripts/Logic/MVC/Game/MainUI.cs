using UnityEngine;
using Config;

namespace Logic
{
    public class MainUI : BaseUI<MainUIComp>
    {

        protected override UISort sort => UISort.FinishUI;
        protected override string prefabName => "Game/MainUI";

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {


        }

        protected override void OnUpdate()
        {
            int maxScore = ConfigManager.instance.gameConfig.maxScore;
            var player0Data = PlayerController.instance.Model.GetPlayerData(0);
            var player1Data = PlayerController.instance.Model.GetPlayerData(1);
            sceneComponent.player0Value.sizeDelta = new Vector2((float)player0Data.score / maxScore * 708, 49);
            sceneComponent.player1Value.sizeDelta = new Vector2((float)player1Data.score / maxScore * 708, 49);

            sceneComponent.player0Text.text = $"分数:{player0Data.score} 阶段:{player0Data.stageConfig.name}";
            sceneComponent.player1Text.text = $"分数:{player1Data.score} 阶段:{player1Data.stageConfig.name}";

            if (player0Data.stage != player1Data.stage)
            {
                var leader = player0Data.stage > player1Data.stage ? player0Data : player1Data;
                sceneComponent.tips.text = $"{(player0Data.stage > player1Data.stage ? "左脑" : "右脑")}阶段领先，{Mathf.CeilToInt(ConfigManager.instance.gameConfig.aheadSuccessNeedTime - GameController.instance.aheadSuccessTimer)}秒后即将获胜";
            }
            else
            {
                sceneComponent.tips.text = "";
            }
        }
    }
}