using UnityEngine;
using Config;

namespace Logic
{
    public class GameUI : BaseUI
    {

        protected override UISort sort => UISort.GameUI;
        protected override string prefabName => "Game/GameUI";
        private GameObject credits;

        protected override void OnInit()
        {
            credits = GetChild("bg");
        }

        protected override void OnShow()
        {
            GameController.instance.PauseGame = true;
        }

        protected override void OnHide()
        {
            GameController.instance.PauseGame = false;
        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "replayLevel":
                    if (SceneController.instance.currentLevelID.IsFilled())
                    {
                        GameController.instance.ReStartCurrentLevel();
                        Hide();
                    }
                    break;
                case "continue":
                    Hide();
                    break;
                case "exit":
                    Application.Quit();
                    break;
                case "credits":
                    credits.SetActiveOptimize(true);
                    break;
                case "close":
                    credits.SetActiveOptimize(false);
                    break;
                case "intro":
                    GameController.instance.ChangeToIntro();
                    Hide();
                    break;
            }

        }
    }
}