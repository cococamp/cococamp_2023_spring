using Config;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Logic
{
    public class IntroUI : BaseUI
    {
        protected override UISort sort => UISort.IntroUI;

        protected override string prefabName => "Game/IntroUI";
        private MultiItemManager levelBtnList;
        private LevelConfigItem[] levelList;

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "btnStart":
                    AudioManager.instance.PlayEffect("start");
                    GameController.instance.LoadLevel("Level1");
                    //sceneObject.GetChild("bg/btnStart").SetActiveOptimize(false);
                    //levelBtnList.gameObject.SetActiveOptimize(true);
                    break;
            }
            if (obj.name.StartsWith("Button"))
            {
                GameController.instance.LoadLevel(obj.GetChild<Text>("Text").text);
            }
        }

        protected override void OnInit()
        {
            levelList = ConfigManager.instance.levelConfig.levelList;
            levelBtnList = sceneObject.GetChild<MultiItemManager>("bg/levelList");
            levelBtnList.SetSize(levelList.Length);
            for (int i = 0; i < levelList.Length; i++)
            {
                var btnTransform = levelBtnList.GetItem<Transform>(i);
                btnTransform.Find("Text").GetComponent<Text>().text = levelList[i].SceneID;
            }
            ResetAllClicks();
        }

        protected override void OnShow()
        {
            sceneObject.GetChild("bg/btnStart").SetActiveOptimize(true);
            levelBtnList.gameObject.SetActiveOptimize(false);
            //sceneObject.GetChild<AnimationPlayer>("bg/train").SetIndex(0);
            sceneObject.GetChild<RectTransform>("bg/train").DOAnchorPosX(0, 3).From(new Vector2(2023, 0)).SetEase(Ease.OutCirc);
            AudioManager.instance.PlayBGM("bgm");
            DOTween.Sequence()
            .AppendCallback(() =>
            {
                sceneObject.GetChild<Image>("bg/btnStart").color = new Color(1, 1, 1, 0);
                sceneObject.GetChild<Text>("bg/btnStart/Text").color = new Color(1, 1, 1, 0);
                AudioManager.instance.PlayAGM("subway");
            })
            .InsertCallback(2, () =>
            {
                AudioManager.instance.StopAGM(1.0f);
            })
            .Append(sceneObject.GetChild<Image>("bg/btnStart").DOFade(1, 1))
            .Join(sceneObject.GetChild<Text>("bg/btnStart/Text").DOFade(1, 1));
        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }
    }
}