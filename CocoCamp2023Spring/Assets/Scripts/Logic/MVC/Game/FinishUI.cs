using UnityEngine;
using Config;

namespace Logic
{
    public class FinishUIData
    {
        public string txt;
        public int star;
        public int bgIndex;
    }
    public class FinishUI : BaseUI<FinishUIComp, FinishUIData>
    {

        protected override UISort sort => UISort.FinishUI;
        protected override string prefabName => "Game/FinishUI";

        protected override void OnInit()
        {
            
        }

        protected override void OnShow()
        {
            sceneComponent.text.text = data.txt;
            sceneComponent.animator.Play(GetAnimNameByInt(data.star));
            sceneComponent.txtBg.SetActiveOptimize(data.star == 4);
            sceneComponent.bg.sprite = sceneComponent.bgs[data.bgIndex];
            GameController.instance.PauseGame = true;
        }

        public string GetAnimNameByInt(int count)
        {
            if(count == 1) return "one";
            if(count == 2) return "two";
            if(count == 3) return "three";
            if(count == 4) return "four";
            return "one";
        }

        protected override void OnHide()
        {
            GameController.instance.PauseGame = false;
        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "btnReplay":
                    if (SceneController.instance.currentLevelID.IsFilled())
                    {
                        GameController.instance.ReStartCurrentLevel();
                        Hide();
                    }
                    break;
                case "btnIntro":
                    GameController.instance.ChangeToIntro();
                    Hide();
                    break;
            }

        }
    }
}