using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Logic
{
    public class GameController : BaseController<GameController, GameModel>
    {
        private bool _pauseGame = false;
        public bool PauseGame
        {
            get => _pauseGame;
            set
            {
                if (_pauseGame != value)
                {
                    _pauseGame = value;
                    //Time.timeScale = value ? 0 : 1;
                }
            }
        }
        private GameOperatorManager operatorManager;
        protected override void OnInit()
        {
            operatorManager = new GameOperatorManager();
            //InitDebugUI();
        }

        protected override void OnDestroy()
        {
            //DestroyDebugUI();
        }
        protected override void RegisterAllUI()
        {
            RegisterUI(UISort.IntroUI, new IntroUI());
            RegisterUI(UISort.GameUI, new GameUI());
            RegisterUI(UISort.FinishUI, new FinishUI());
            RegisterUI(UISort.MainUI, new MainUI());
        }

        protected override void RegisterEvents()
        {
            EventManager.Get<EventDefine.ModulesInitFinished>().AddListener(OnModulesInitFinished);
            EventManager.Get<EventDefine.InputKeyboardEvent>().AddListener(OnButtonClick);
        }

        protected override void UnRegisterEvents()
        {
            EventManager.Get<EventDefine.ModulesInitFinished>().RemoveListener(OnModulesInitFinished);
            EventManager.Get<EventDefine.InputKeyboardEvent>().RemoveListener(OnButtonClick);
        }

        protected override void OnFixedUpdate()
        {
            Model.FixedUpdate();
            UpdateFinish();
            //UpdateDebugUI();
        }

        private void OnButtonClick(InputAction.CallbackContext context)
        {
            if (context.action.name == "Escape")
            {
                OpenGameUI();
            }
        }

        private void OpenGameUI()
        {
            //提交分数的时候  不能打开这个界面
            if (LeaderBoardController.instance.GetUI(UISort.InputNameUI)?.isShow == false)
            {
                if (GetUI<GameUI>(UISort.GameUI).isShow)
                {
                    HideUI(UISort.GameUI);
                }
                else
                {
                    ShowUI(UISort.GameUI);
                }
            }
        }

        private void OnModulesInitFinished()
        {
            operatorManager.Init();
        }

        public void ReStartCurrentLevel()
        {
            LoadLevel(SceneController.instance.currentLevelID);
        }

        public void LoadLevel(string levelID)
        {
            operatorManager.ChangeState(GameOperatorState.Level, new GameOperatorParam_Level() { levelID = levelID });
        }

        public void ChangeToIntro()
        {
            operatorManager.ChangeState(GameOperatorState.Intro);
        }

        #region Finish
        public bool IsFinish { get; private set; } = true;
        public float aheadSuccessTimer = 0;

        public void InitFinish()
        {
            IsFinish = false;
            aheadSuccessTimer = 0;
        }
        private void UpdateFinish()
        {
            if (IsFinish) return;

            int stage1 = PlayerController.instance.Model.GetPlayerData(0)?.stage ?? 0;
            int stage2 = PlayerController.instance.Model.GetPlayerData(1)?.stage ?? 0;
            if (stage1 != stage2)
            {
                aheadSuccessTimer += Time.fixedDeltaTime;
                if (aheadSuccessTimer > Config.ConfigManager.instance.gameConfig.aheadSuccessNeedTime)
                {
                    IsFinish = true;
                    ShowUI(UISort.FinishUI, new FinishUIData()
                    {
                        txt = $"{(stage1 > stage2 ? "左脑" : "右脑")}阶段领先获胜",
                        star = 2,
                        bgIndex = stage1 > stage2 ? 3 : 2,
                    });
                }
            }
            else
            {
                aheadSuccessTimer = 0;
            }

            int score1 = PlayerController.instance.Model.GetPlayerData(0)?.score ?? 0;
            int score2 = PlayerController.instance.Model.GetPlayerData(1)?.score ?? 0;
            if (score1 >= Config.ConfigManager.instance.gameConfig.maxScore
            || score2 >= Config.ConfigManager.instance.gameConfig.maxScore)
            {
                IsFinish = true;
                ShowUI(UISort.FinishUI, new FinishUIData()
                {
                    txt = $"失败",
                    star = 1,
                    bgIndex = 0,
                });
                //ShowUI(UISort.FinishUI, $"一星\n分数爆掉结束\nPlayer1 score:{score1}\n Player2 score:{score2}");
            }

            if (Model.gameTime >= Config.ConfigManager.instance.gameConfig.gameTime)
            {
                IsFinish = true;
                if (score1 == 0 && score2 == 0)
                {
                    ShowUI(UISort.FinishUI, new FinishUIData()
                    {
                        txt = $"和平胜利",
                        star = 4,
                        bgIndex = 1,
                    });
                    //ShowUI(UISort.FinishUI, $"四星\n和平结束\nPlayer1 score:{score1}\n Player2 score:{score2}");
                }
                else
                {
                    if (score1 == score2)
                    {
                        ShowUI(UISort.FinishUI, new FinishUIData()
                        {
                            txt = $"{(score1 > score2 ? "左脑" : "右脑")}分数领先获胜",
                            star = 3,
                            bgIndex = score1 > score2 ? 3 : 2,
                        });
                        //ShowUI(UISort.FinishUI, $"三星\n完成时长结束\nSuccess  Player1 score:{score1}\n Player2 score:{score2}\nWinner:Player{(score1 > score2 ? 0 : 1)}\n但这一切值得吗");
                    }
                    else
                    {
                        ShowUI(UISort.FinishUI, new FinishUIData()
                        {
                            txt = $"平局",
                            star = 3,
                            bgIndex = 0,
                        });
                        //ShowUI(UISort.FinishUI, $"三星\n完成时长结束\nSuccess  Player1 score:{score1}\n Player2 score:{score2}");
                    }
                }
            }
        }
        #endregion

        #region Debug
        private DebugUI debugUI;
        private void InitDebugUI()
        {
            debugUI = DebugController.instance.CreateDebugUI(() =>
            {
                string[] logs = new string[]
                {
                    $"gameState:{operatorManager.currentState}",
                    $"gameTime:{Model.gameTime}/{Config.ConfigManager.instance.gameConfig.gameTime}",
                    $"station:{SceneController.instance.stationEntity?.currentStation ?? 0}/{Config.ConfigManager.instance.gameConfig.stationStartTimes.Count}",
                    $"aheadSuccessTimer:{aheadSuccessTimer}",
                    $"IsFinish:{IsFinish}",
                    $"IsPause:{PauseGame}",
                };
                return string.Join("\n", logs);
            }, new Vector2(0, 0));
        }

        private void DestroyDebugUI()
        {
            DebugController.instance.DestroyDebugUI(debugUI);
            debugUI = null;
        }

        private void UpdateDebugUI()
        {
            debugUI.Update();
        }

        #endregion
    }
}