using UnityEngine;
using UnityEngine.InputSystem;
using Config;
using DG.Tweening;
using UnityEngine.Rendering.PostProcessing;

namespace Logic
{
    public class PlayAreaEntity : BaseEntity<PlayAreaEntityComp>
    {
        private int currentStage = 0;
        public float Radius => ConfigManager.instance.playAreaConfig.animConfigs[currentStage].radius;
        public Vector2 CenterPos => ConfigManager.instance.playAreaConfig.animConfigs[currentStage].center;

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {
            targetPostProcessValue = 0;
            currentPostProcessValue = 0;
            currentStage = 0;
            InitSprites();
            InitColliders();
        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {

        }

        private float targetPostProcessValue = 0;
        private float currentPostProcessValue = 0;
        protected override void OnUpdate()
        {
            if (PlayerController.instance.Model.GetMinScorePlayerData().stage > currentStage)
            {
                ChangeStage();
            }

            targetPostProcessValue = (float)PlayerController.instance.Model.GetMaxScorePlayerData().score / ConfigManager.instance.gameConfig.maxScore;
            currentPostProcessValue = Mathf.Lerp(currentPostProcessValue, targetPostProcessValue, 0.2f * Time.deltaTime);
            Cameras.CameraManager.instance.postProcessProfile.GetSetting<ChromaticAberration>().intensity.Override(currentPostProcessValue);
            Cameras.CameraManager.instance.postProcessProfile.GetSetting<ColorGrading>().mixerRedOutBlueIn.Override(100 * currentPostProcessValue);
        }

        public void ChangeStage()
        {
            if (currentStage > 2) return;
            DoAnim(currentStage);
            currentStage++;
        }

        private void DoAnim(int stage)
        {
            //切换碰撞体
            sceneComponent.colliders[stage].SetActiveOptimize(false);
            sceneComponent.colliders[stage + 1].SetActiveOptimize(true);

            var animConfig = ConfigManager.instance.playAreaConfig.animConfigs[stage];
            DOTween.Sequence()
            .Append(sceneComponent.brains[stage].transform.DOScale(animConfig.currentMaxScale, animConfig.currentScaleTime).SetEase(Ease.OutElastic))
            .Insert(animConfig.nextFadeInStartTime, sceneComponent.brains[stage + 1].DOFade(1, animConfig.nextFadeInNeedTime))
            .Join(sceneComponent.brains[stage].DOFade(0, animConfig.nextFadeInNeedTime));
        }

        private void InitSprites()
        {
            foreach (var sprite in sceneComponent.brains)
            {
                sprite.SetAlpha(0);
                sprite.transform.localScale = Vector3.one;
            }
            sceneComponent.brains[0].SetAlpha(1);
        }

        private void InitColliders()
        {
            foreach (var collider in sceneComponent.colliders)
            {
                collider.SetActiveOptimize(false);
            }
            sceneComponent.colliders[0].SetActiveOptimize(true);
        }
    }
}