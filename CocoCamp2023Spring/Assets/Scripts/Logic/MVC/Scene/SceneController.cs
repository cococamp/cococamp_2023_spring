using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using Cameras;

namespace Logic
{
    public class SceneController : BaseController<SceneController>
    {
        public SceneObjectRegister sceneObjectRegister;
        public string currentLevelID;
        private string loadingLevelID;
        public bool IsInit => currentLevelID.IsFilled();
        protected override void OnInit()
        {

        }

        protected override void RegisterAllUI()
        {

        }
        public void LoadScene(string LevelID)
        {
            if (loadingLevelID.IsFilled())
            {
                Debug.LogError($"正在加载场景{loadingLevelID}中  无法加载{LevelID}");
                return;
            }

            if (currentLevelID.IsFilled())
            {
                Debug.LogError($"当前已在场景{currentLevelID}中  无法加载{LevelID}  请先Release之前的场景");
                return;
            }

            CoroutineManager.instance.TryStartCoroutine(I_LoadScene(LevelID));
        }

        private IEnumerator I_LoadScene(string levelID)
        {
            Debug.Log($"Start LoadScene {levelID}");
            loadingLevelID = levelID;
            var op = SceneManager.LoadSceneAsync(levelID);
            while (!op.isDone)
            {
                yield return null;
            }
            loadingLevelID = null;
            Debug.Log($"Finish LoadScene {levelID}");

            InitScene(levelID);
        }

        public void InitScene(string levelID)
        {
            Debug.Log("InitScene");
            sceneObjectRegister = Transform.FindObjectOfType<SceneObjectRegister>();
            if (sceneObjectRegister == null)
            {
                Debug.LogError("未找到 SceneObjectRegister   请添加");
                return;
            }
            OnInit(levelID);
            currentLevelID = levelID;
            EventManager.Get<EventDefine.SceneInitFinished>().Fire();
        }

        public void ReleaseScene()
        {
            if (!IsInit) return;
            OnRelease();
            currentLevelID = null;
            sceneObjectRegister = null;
        }

        public PlayAreaEntity playAreaEntity;
        public StationEntity stationEntity;

        private void OnInit(string levelID)
        {
            playAreaEntity = new PlayAreaEntity();
            playAreaEntity.InitWithGameObject(sceneObjectRegister.playArea.gameObject);

            stationEntity = new StationEntity();
            stationEntity.InitWithGameObject(sceneObjectRegister.station.gameObject);

            PlayerController.instance.CreatePlayers();

            GameController.instance.Model.StartGameTime();
            GameController.instance.InitFinish();
            OptionController.instance.StartCreateTimer();

            AudioManager.instance.PlayBGM("bgm");

            GameController.instance.ShowUI(UISort.MainUI);
        }

        private void OnRelease()
        {
            GameController.instance.HideUI(UISort.MainUI);
            AudioManager.instance.StopBGM();
            OptionController.instance.StopCreateTimer();
            OptionController.instance.DestroyAllOption();
            GameController.instance.Model.StopGameTime();

            PlayerController.instance.DestroyPlayers();

            playAreaEntity.Destroy();
            playAreaEntity = null;

            stationEntity.Destroy();
            stationEntity = null;
        }

        protected override void OnFixedUpdate()
        {
            if (!IsInit) return;

            playAreaEntity.Update();
            stationEntity.Update();
        }
    }
}