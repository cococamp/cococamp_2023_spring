using UnityEngine;
using UnityEngine.InputSystem;
using Config;
using DG.Tweening;

namespace Logic
{
    public class StationEntity : BaseEntity<StationEntityComp>
    {
        public int currentStation = 0;
        protected override void OnInit()
        {
            currentStation = 0;
            foreach (var item in sceneComponent.stations)
            {
                item.SetActiveOptimize(false);
            }
            sceneComponent.stations[currentStation].SetActiveOptimize(true);
        }
        protected override void OnShow()
        {
            AudioManager.instance.PlayAGM("subway");
        }
        protected override void OnHide()
        {
            AudioManager.instance.StopAGM();
        }

        protected override void OnDestroy()
        {

        }
        protected override void OnClick(GameObject obj)
        {

        }
        protected override void OnUpdate()
        {
            if (currentStation > 2) return;
            if (GameController.instance.Model.gameTime > ConfigManager.instance.gameConfig.stationStartTimes[currentStation + 1])
            {
                currentStation++;
                DOTween.Sequence()
                .AppendCallback(() =>
                {
                    AudioManager.instance.StopAGM();
                })
                .InsertCallback(1, () =>
                {
                    sceneComponent.stations[currentStation].SetActiveOptimize(true);
                    AudioManager.instance.PlayEffect("station2");
                })
                .InsertCallback(5, () =>
                {
                    AudioManager.instance.PlayAGM("subway");
                });
            }
        }
    }
}