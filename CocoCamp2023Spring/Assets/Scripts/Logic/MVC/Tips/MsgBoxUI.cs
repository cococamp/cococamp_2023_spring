using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System;

namespace Logic
{
    public struct MsgBoxUIData
    {
        public string content;
        public bool isShowCancelBtn;
        public Action callbackOK;
        public Action callbackCancel;

    }
    public class MsgBoxUI : BaseUI<MsgBoxUIComp, MsgBoxUIData>
    {
        protected override UISort sort => UISort.MsgBoxUI;

        protected override string prefabName => "Tips/MsgBoxUI";

        protected override void OnClick(GameObject obj)
        {
            if(obj.name == "OK")
            {
                data.callbackOK?.Invoke();
                Hide();
            }
            else if(obj.name == "Cancel")
            {
                data.callbackCancel?.Invoke();
                Hide();
            }
        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {
            sceneComponent.content.text = data.content;
            sceneComponent.btnCancel.SetActiveOptimize(data.isShowCancelBtn);
        }
    }
}