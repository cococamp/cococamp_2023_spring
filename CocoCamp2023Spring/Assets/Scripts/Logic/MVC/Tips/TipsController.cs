using System.Collections.Generic;
using Config;
using UnityEngine;
using System;

namespace Logic
{
    public class TipsController : BaseController<TipsController>
    {
        protected override void OnInit()
        {
            
        }
        protected override void RegisterAllUI()
        {
            RegisterUI(UISort.MsgBoxUI, new MsgBoxUI());
        }
        public void ShowTips(string tip)
        {
            var tipsUI = new TipsUI();
            tipsUI.Init();
            tipsUI.Show(tip);
        }

        public void ShowMsgBox(MsgBoxUIData data)
        {
            ShowUI(UISort.MsgBoxUI, data);
        }
    }
}