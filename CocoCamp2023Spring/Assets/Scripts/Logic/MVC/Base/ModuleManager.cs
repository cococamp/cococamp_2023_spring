using System.Collections.Generic;

namespace Logic
{
    public class ModuleManager : SingletonClass<ModuleManager>
    {
        private void RegisterAllModules()
        {
            RegisterModule(GameController.instance);
            RegisterModule(DebugController.instance);
            RegisterModule(TipsController.instance);
            RegisterModule(LeaderBoardController.instance);
            RegisterModule(PlayerController.instance);
            RegisterModule(OptionController.instance);
            RegisterModule(SceneController.instance);
        }

        private HashSet<IController> moduleList;
        private void RegisterModule(IController controller)
        {
            controller.Init();
            moduleList.Add(controller);
        }

        public void InitAllModules()
        {
            moduleList = new HashSet<IController>();
            RegisterAllModules();
            EventManager.Get<EventDefine.ModulesInitFinished>().Fire();
        }

        public void Update()
        {
            GameController.instance.Update();
        }

        public void FixedUpdate()
        {
            foreach (var controller in moduleList)
            {
                controller.FixedUpdate();
            }
        }

        public void DestroyAllModules()
        {
            foreach (var controller in moduleList)
            {
                controller.Destroy();
            }
        }
    }
}