namespace Logic
{
    //序号越高  在UI里的层级越高
    public enum UISort
    {
        IntroUI,
        MainUI,
        FinishUI,
        LeaderBoardUI,
        SubmitUI,
        InputNameUI,
        GameUI,

        #region Tips
        MsgBoxUI,
        TipsUI,
        #endregion

        #region Debug
        DebugUI,

        #endregion
    }
}