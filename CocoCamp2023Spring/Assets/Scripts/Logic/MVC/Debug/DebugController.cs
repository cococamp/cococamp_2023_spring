using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Logic
{
    public class DebugController : BaseController<DebugController, DebugModel>
    {
        protected override void OnInit()
        {

        }
        protected override void OnFixedUpdate()
        {
#if UNITY_EDITOR
            if (Keyboard.current.f1Key.wasPressedThisFrame)
            {
                SceneController.instance.playAreaEntity.ChangeStage();
            }

            if (Keyboard.current.f2Key.wasPressedThisFrame)
            {

            }
#endif
        }

        public DebugUI CreateDebugUI(Func<string> logGetter, Vector2 alignment)
        {
            var debugUI = new DebugUI();
            debugUI.Init(logGetter, alignment);
            debugUI.Show();
            return debugUI;
        }

        public void DestroyDebugUI(DebugUI debugUI)
        {
            debugUI.Destroy();
        }
    }
}