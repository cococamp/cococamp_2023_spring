using UnityEngine;
using UnityEngine.UI;
using Config;

namespace Logic
{
    public class SubmitUI : BaseUI<SubmitUIComp, int>
    {
        protected override string prefabName => "LeaderBoard/SubmitUI";

        protected override UISort sort => UISort.SubmitUI;
        protected override void OnInit()
        {
        }

        protected override void OnShow()
        {
            sceneComponent.scoreText.text = $"得分：{data}";
            RefreshName();
        }

        private void RefreshName()
        {
            if (LeaderBoardController.instance.Model.GetName(out string name))
            {
                sceneComponent.nameText.text = name;
            }
            else
            {
                sceneComponent.nameText.text = "";
            }
        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }
        protected override void RegisterEvents()
        {
            EventManager.Get<EventDefine.NameRefreshEvent>().AddListener(RefreshName);
            EventManager.Get<EventDefine.UploadScoreSuccessEvent>().AddListener(OnUploadScoreSuccess);
        }
        protected override void UnRegisterEvents()
        {
            EventManager.Get<EventDefine.UploadScoreSuccessEvent>().RemoveListener(OnUploadScoreSuccess);
            EventManager.Get<EventDefine.NameRefreshEvent>().RemoveListener(RefreshName);
        }

        protected override void OnUpdate()
        {

        }

        protected override void OnClick(GameObject obj)
        {
            if (obj.name == "btnSubmit")
            {
                var highScore = LeaderBoardController.instance.Model.GetHighScore();
                if (data > highScore)
                {
                    LeaderBoardController.instance.Model.SaveHighScore(data);
                }
                else
                {
                    Hide();
                    LeaderBoardController.instance.ShowUI(UISort.LeaderBoardUI);
                }
            }
        }

        private void OnUploadScoreSuccess(int score)
        {
            if (score == data)
            {
                Hide();
                LeaderBoardController.instance.ShowUI(UISort.LeaderBoardUI);
            }
        }
    }
}