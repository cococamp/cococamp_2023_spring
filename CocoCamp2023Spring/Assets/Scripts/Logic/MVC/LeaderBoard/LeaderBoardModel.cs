using UnityEngine;
using System.Collections.Generic;
using System;

namespace Logic
{
    public class LeaderBoardModel : BaseModel
    {
        private const string PrefsKey_Name = "Creep_Name";
        private const string PrefsKey_HighScore = "Creep_HighScore";
        private const string PrefsKey_HighScoreTime = "Creep_HighScoreTime";
        private List<UserData> m_userDataList;
        private const float requestTime = 5f;
        private float requestTimeCounter = requestTime;
        public override void Init()
        {
            requestTimeCounter = 0;
        }

        public override void Destroy()
        {

        }

        public bool GetName(out string name)
        {
            name = PlayerPrefs.GetString(PrefsKey_Name);
            return !name.IsNullOrEmpty();
        }

        public void SaveName(string name)
        {
            var currentName = PlayerPrefs.GetString(PrefsKey_Name);
            if (currentName.IsNullOrEmpty() && !name.IsNullOrEmpty())
            {
                PlayerPrefs.SetString(PrefsKey_Name, name);
                EventManager.Get<EventDefine.NameRefreshEvent>().Fire();
            }
        }

        public void RequestAllUserData()
        {
            if(requestTimeCounter >= 0) return;
            LeaderBoardService.instance.GetAllScore((success, dataList) =>
            {
                if (success)
                {
                    requestTimeCounter = requestTime;
                    m_userDataList = dataList;
                    //TODO sort

                    EventManager.Get<EventDefine.LeaderBoardRefreshEvent>().Fire();
                }
            });
        }

        public List<UserData> GetAllUserData()
        {
            return m_userDataList;
        }

        public void SaveHighScore(int score)
        {
            var oldScore = GetHighScore();
            if (score > oldScore)
            {
                PlayerPrefs.SetInt(PrefsKey_HighScore, score);
                PlayerPrefs.SetString(PrefsKey_HighScoreTime, DateTime.Now.ToString());
                if (GetName(out string name))
                {
                    LeaderBoardService.instance.AddScore(name, score, (success) =>
                    {
                        EventManager.Get<EventDefine.UploadScoreSuccessEvent>().Fire(score);
                    });
                }
                else
                {
                    TipsController.instance.ShowTips("请先注册一个名字！");
                }
            }
        }

        public int GetHighScore()
        {
            int localScore = PlayerPrefs.GetInt(PrefsKey_HighScore);
            return localScore;
        }

        public DateTime GetHighScoreTime()
        {
            var time = PlayerPrefs.GetString(PrefsKey_HighScoreTime);
            if (time.IsNullOrEmpty())
            {
                return DateTime.MinValue;
            }
            return DateTime.Parse(time);
        }

        public void FixedUpdate()
        {
            if (requestTimeCounter >= 0)
            {
                requestTimeCounter -= Time.fixedDeltaTime;
            }
        }
    }
}