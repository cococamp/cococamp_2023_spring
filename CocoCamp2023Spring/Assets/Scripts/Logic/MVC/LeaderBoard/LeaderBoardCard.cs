using System;
using UnityEngine;
using UnityEngine.UI;
namespace Logic
{
    public class LeaderBoardCard : MonoBehaviour
    {
        public Image bg;
        public Text indexText;
        public Text txtName;
        public Text txtScore;
        public Text txtTime;

        public Color normalColor;
        public Color mineColor;

        public void SetData(int index, string name, int score, DateTime time, bool self = false, bool isOutOfRank = false)
        {
            indexText.text = isOutOfRank ? $"{index}+" : index.ToString();
            txtName.text = name;
            txtScore.text = score.ToString();
            txtTime.text = time.ToString();

            bg.color = self ? mineColor : normalColor;
        }
    }
}