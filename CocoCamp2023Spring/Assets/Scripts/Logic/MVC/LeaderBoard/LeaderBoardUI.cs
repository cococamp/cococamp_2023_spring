using UnityEngine;
using UnityEngine.UI;
using Config;

namespace Logic
{
    public class LeaderBoardUI : BaseUI<LeaderBoardUIComp>
    {
        protected override string prefabName => "LeaderBoard/LeaderBoardUI";

        protected override UISort sort => UISort.LeaderBoardUI;
        protected override void OnInit()
        {
        }

        protected override void OnShow()
        {
            LeaderBoardController.instance.Model.RequestAllUserData();
            RefreshList();
        }

        private void RefreshList()
        {
            LeaderBoardController.instance.Model.GetName(out string myName);
            var myScore = LeaderBoardController.instance.Model.GetHighScore();
            var dataList = LeaderBoardController.instance.Model.GetAllUserData();
            if (dataList == null)
            {
                sceneComponent.List.SetSize(0);
                return;
            }

            bool haveMyData = false;
            sceneComponent.List.SetSize(dataList.Count);
            for (int i = 0; i < dataList.Count; i++)
            {
                var data = dataList[i];
                var card = sceneComponent.List.GetItem<LeaderBoardCard>(i);
                bool isMyData = data.IsMyData(myName, myScore);
                if (isMyData) haveMyData = true;
                card.SetData(i + 1, data.text, data.score, data.date.AddHours(8), isMyData);
            }

            //我的排名
            if (!haveMyData)
            {
                sceneComponent.MyCardRoot.SetActiveOptimize(true);
                sceneComponent.MyCardRoot.transform.SetAsLastSibling();
                var highScoreTime = LeaderBoardController.instance.Model.GetHighScoreTime();
                sceneComponent.MyCard.SetData(dataList.Count, myName, myScore, highScoreTime, true, true);
            }
            else
            {
                sceneComponent.MyCardRoot.SetActiveOptimize(false);
            }
        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }
        protected override void RegisterEvents()
        {
            EventManager.Get<EventDefine.LeaderBoardRefreshEvent>().AddListener(RefreshList);
        }
        protected override void UnRegisterEvents()
        {
            EventManager.Get<EventDefine.LeaderBoardRefreshEvent>().RemoveListener(RefreshList);
        }

        protected override void OnUpdate()
        {

        }
        protected override void OnClick(GameObject obj)
        {
            if (obj.name == "btnRefresh")
            {
                LeaderBoardController.instance.Model.RequestAllUserData();
            }
            else if (obj.name == "btnRestart")
            {
                GameController.instance.ReStartCurrentLevel();
                Hide();
            }
        }
    }
}