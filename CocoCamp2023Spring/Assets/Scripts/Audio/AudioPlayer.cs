using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class AudioPlayer : MonoBehaviour
{
    public AudioSource source;
    public bool isPlaying => source.isPlaying;
    [Range(0, 1)]
    public float maxVolume = 1.0f;

    void Awake()
    {
        source = GetComponent<AudioSource>();
        source.volume = 0;
    }

    public void Play(AudioClip clip, bool loop = false, float fadeTime = 0.1f)
    {
        if (source.clip != clip)
        {
            if (source.isPlaying && source.volume > 0)
            {
                source.loop = loop;
                StopAllCoroutines();
                StartCoroutine(I_Change(clip, fadeTime * 2, fadeTime));
            }
            else
            {
                source.volume = 0;
                source.clip = clip;
                source.loop = loop;
                source.Play();
                StopAllCoroutines();
                StartCoroutine(I_FadeIn(fadeTime));
            }
        }
        else
        {
            if (!source.isPlaying || source.volume == 0)
            {
                source.volume = 0;
                source.loop = loop;
                source.Play();
                StopAllCoroutines();
                StartCoroutine(I_FadeIn(fadeTime));
            }
        }
    }

    public void Stop(float fadeTime = 0.2f)
    {
        StopAllCoroutines();
        StartCoroutine(I_FadeOut(fadeTime));
    }

    IEnumerator I_FadeIn(float time)
    {
        if (time <= 0)
        {
            source.volume = maxVolume;
        }
        else
        {
            while (source.volume < maxVolume)
            {
                source.volume += maxVolume * (Time.deltaTime / time);
                yield return null;
            }
        }
    }

    IEnumerator I_FadeOut(float time)
    {
        if (time <= 0)
        {
            source.volume = 0.0f;
        }
        else
        {
            while (source.volume > 0)
            {
                source.volume -= maxVolume * (Time.deltaTime / time);
                yield return null;
            }
        }
    }

    IEnumerator I_Change(AudioClip clip, float fadeOutTime, float fadeInTime)
    {
        yield return I_FadeOut(fadeOutTime);
        source.clip = clip;
        source.Play();
        yield return I_FadeIn(fadeInTime);
    }
}