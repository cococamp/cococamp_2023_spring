using UnityEngine;
using UnityEngine.InputSystem;
using Logic;

namespace Input
{
    public enum PlayerIndex
    {
        Player1,
        Player2
    }
    public class InputManager : SingletonMonoBehaviourClass<InputManager>
    {
        public PlayerInput playerGamePadInput1;
        public PlayerInput playerGamePadInput2;
        public PlayerInput keyBoardInput;
        protected override void onAwake()
        {
            InputSystem.onDeviceChange += onDeviceChange;

            playerGamePadInput1.onActionTriggered += (InputAction.CallbackContext context) =>
            {
                HandleInput(0, context);
            };
            playerGamePadInput2.onActionTriggered += (InputAction.CallbackContext context) =>
            {
                HandleInput(1, context);
            };
            keyBoardInput.onActionTriggered += (InputAction.CallbackContext context) =>
            {
                if (context.action.name.StartsWith("Player"))
                {
                    if (context.action.name.EndsWith("1"))
                    {
                        HandleInput(0, context);
                    }
                    else if (context.action.name.EndsWith("2"))
                    {
                        HandleInput(1, context);
                    }
                }
                else if(context.action.type == InputActionType.Button)
                {
                    EventManager.Get<Logic.EventDefine.InputKeyboardEvent>().Fire(context);
                }
            };
        }

        private void onDeviceChange(InputDevice device, InputDeviceChange deviceChange)
        {
            //Debug.LogError($"OnDeviceChanged  {device.deviceId}  deviceChange:{deviceChange}");
        }

        //处理角色控制
        private void HandleInput(int playerIndex, InputAction.CallbackContext context)
        {
            //暂停游戏时  不能进行玩家操作
            if(GameController.instance.PauseGame) return;
            //Debug.LogError($"player{playerIndex}  {context.ToString()}");
            EventManager.Get<Logic.EventDefine.InputEvent>().Fire(playerIndex, context);
        }
    }
}