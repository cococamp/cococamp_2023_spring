using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Config
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "CreateConfig/GameConfig")]
    [Serializable]
    public class GameConfig : ScriptableObject
    {
        [LabelText("游戏时间(秒)")]
        public int gameTime;
        [LabelText("各站到达时间")]
        public List<int> stationStartTimes;

        [LabelText("最大分数")]
        [OnValueChanged("OnMaxScoreChanged")]
        public int maxScore;
        [LabelText("阶段数量")]
        [MinValue(1)]
        [MaxValue(10)]
        [OnValueChanged("OnStageCountChanged")]
        public int stageCount;

        [HideInInspector]
        public List<int> stageDivideTime;

        [LabelText("阶段划分")]
        [TableList(ShowIndexLabels = true, IsReadOnly = true)]
        public List<StageConfig> stageDivideShow;

        [OnInspectorInit]
        private void Init()
        {
            stageDivideTime ??= new List<int>();
            RefreshStageShow();
        }

        private void OnStageCountChanged()
        {
            while (stageDivideTime.Count < stageCount - 1)
            {
                stageDivideTime.Add(maxScore);
            }

            stageDivideTime.RemoveRange(stageCount - 1, stageDivideTime.Count - (stageCount - 1));
            RefreshStageShow();
        }

        private void OnMaxScoreChanged()
        {
            for (int i = 0; i < stageDivideTime.Count; i++)
            {
                stageDivideTime[i] = Mathf.Min(stageDivideTime[i], maxScore);
            }
            RefreshStageShow();
        }

        private void OnStageValueChanged(int index, int minValue, int maxValue)
        {
            if (index > 0)
            {
                stageDivideTime[index - 1] = minValue;
            }
            if (index < stageDivideTime.Count)
            {
                stageDivideTime[index] = maxValue;
            }
            stageDivideTime.Sort();
            RefreshStageShow();
        }

        private void RefreshStageShow()
        {
            int showCount = stageDivideTime.Count + 1;
            stageDivideShow ??= new List<StageConfig>();
            for (int i = 0; i < showCount; i++)
            {
                int min = (i - 1 < 0) ? 0 : stageDivideTime[i - 1];
                int max = i >= stageDivideTime.Count ? maxScore : stageDivideTime[i];
                if (i >= stageDivideShow.Count)
                {
                    stageDivideShow.Add(new StageConfig()
                    {
                        index = i,
                        maxValue = maxScore,
                        scoreFiled = new Vector2Int(min, max),
                        onValueChanged = OnStageValueChanged
                    });
                }
                else
                {
                    stageDivideShow[i].index = i;
                    stageDivideShow[i].maxValue = maxScore;
                    stageDivideShow[i].scoreFiled = new Vector2Int(min, max);
                    stageDivideShow[i].onValueChanged = OnStageValueChanged;
                }
            }
            stageDivideShow.RemoveRange(showCount, stageDivideShow.Count - showCount);
        }

        [LabelText("分数减少速度(score/s)")]
        [MinValue(0)]
        public float scoreReduceSpeed;

        [LabelText("阶段领先胜利所需时长(秒)")]
        [MinValue(0)]
        public int aheadSuccessNeedTime;

        public int GetStageIndexByScore(int score)
        {
            for (int i = 0; i < stageDivideShow.Count; i++)
            {
                var config = stageDivideShow[i];
                if (score >= config.scoreFiled.x && score < config.scoreFiled.y)
                {
                    return i;
                }
            }
            return 0;
        }

        public StageConfig GetStageConfigByScore(int score)
        {
            int stage = GetStageIndexByScore(score);
            return stageDivideShow[stage];
        }
    }

    [Serializable]
    public class StageConfig
    {
        [VerticalGroup("名字")]
        [TableColumnWidth(70, false)]
        [HideLabel]
        public string name;
        [HideInInspector]
        public int index;
        [HideInInspector]
        [NonSerialized]
        public int maxValue;
        [VerticalGroup("阶段")]
        [HideLabel]
        [MinMaxSlider(0, "maxValue", true)]
        [OnValueChanged("OnValueChanged")]
        public Vector2Int scoreFiled;

        [VerticalGroup("惯性大小")]
        [TableColumnWidth(120, false)]
        [HideLabel]
        [PropertyRange(0, maxInertia)]
        public float inertia;

        [VerticalGroup("创建选项组时间间隔(秒)")]
        [TableColumnWidth(150, false)]
        [HideLabel]
        public float createOptionInterval = 5;

        public const float maxInertia = 1.0f;

        private void OnValueChanged()
        {
            onValueChanged?.Invoke(index, scoreFiled.x, scoreFiled.y);
        }
        [HideInInspector]
        [NonSerialized]
        public Action<int, int, int> onValueChanged;
    }
}

