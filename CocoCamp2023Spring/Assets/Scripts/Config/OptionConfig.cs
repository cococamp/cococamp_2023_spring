using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Logic;
using System.Linq;

namespace Config
{
    [CreateAssetMenu(fileName = "OptionConfig", menuName = "CreateConfig/OptionConfig")]
    [Serializable]
    public class OptionConfig : ScriptableObject
    {
        [Button("刷新")]
        [PropertyOrder(-1)]
        private void Refresh()
        {
            OptionGroup.InitOptionDropDownList();
        }
        [TableList(ShowIndexLabels = true, ShowPaging = true, NumberOfItemsPerPage = 10)]
        [LabelText("选项组配置")]
        public List<OptionGroup> groupList;

        [LabelText("选项配置")]
        [TableList(ShowIndexLabels = true, ShowPaging = true, NumberOfItemsPerPage = 10)]
        public List<OptionItemConfig> itemList;

        public OptionGroup GetRandomOptionGroupByStage(int stage)
        {
            return groupList.Where((group) =>
            {
                switch (group.belongStageType)
                {
                    case OptionBelongStageType.Single:
                        return group.belongStage == stage;
                    case OptionBelongStageType.Range:
                        return (stage >= group.belongStageRange.x) && (stage <= group.belongStageRange.y);
                    default:
                        return false;
                }
            }).ToList().RandomPick();
        }
        [LabelText("选项通用配置")]
        public OptionCommonConfig commonConfig;
    }

    [Serializable]
    public class OptionGroup
    {
        // [VerticalGroup("组ID")]
        // [TableColumnWidth(80, false)]
        // [HideLabel]
        // public string groupId;

        [VerticalGroup("所属阶段")]
        [TableColumnWidth(160, false)]
        [HideLabel]
        [OnValueChanged("RefreshBelongStageName")]
        public OptionBelongStageType belongStageType = OptionBelongStageType.Single;

        [VerticalGroup("所属阶段")]
        [HideLabel]
        [PropertyRange(0, "_maxStage")]
        [ShowIf("belongStageType", OptionBelongStageType.Single)]
        [OnValueChanged("RefreshBelongStageName")]
        public int belongStage;

        [VerticalGroup("所属阶段")]
        [HideLabel]
        [MinMaxSlider(0, "_maxStage", true)]
        [ShowIf("belongStageType", OptionBelongStageType.Range)]
        [OnValueChanged("RefreshBelongStageName")]
        public Vector2Int belongStageRange;

        [VerticalGroup("所属阶段")]
        [ReadOnly]
        [ShowInInspector]
        [HideLabel]
        private string belongStageName;

        private void RefreshBelongStageName()
        {
            switch (belongStageType)
            {
                case OptionBelongStageType.Single:
                    belongStageName = ConfigManager.instance.gameConfig.stageDivideShow[belongStage].name;
                    break;
                case OptionBelongStageType.Range:
                    belongStageName = "";
                    for (int i = belongStageRange.x; i <= belongStageRange.y; i++)
                    {
                        belongStageName += ConfigManager.instance.gameConfig.stageDivideShow[i].name;
                        belongStageName += "  ";
                    }
                    break;
                default:
                    belongStageName = "NULL";
                    break;
            }
        }

        [OnInspectorInit]
        private void OnInspectorInit()
        {
            RefreshBelongStageName();
            InitOptionDropDownList();
        }


        private int _maxStage => ConfigManager.instance.gameConfig.stageCount - 1;

        [VerticalGroup("选项配置")]
        [ValueDropdown("_optionDropDownList")]
        [InfoBox("同一组不能有相同的选项", InfoMessageType.Error, "CheckListInValid")]
        public List<int> list;

        private HashSet<int> _checkHashSet;
        private bool CheckListInValid()
        {
            if (list == null) return false;
            _checkHashSet ??= new HashSet<int>();
            _checkHashSet.Clear();
            foreach (var item in list)
            {
                if (_checkHashSet.Contains(item))
                {
                    return true;
                }
                else
                {
                    _checkHashSet.Add(item);
                }
            }
            return false;
        }

        private static ValueDropdownList<int> _optionDropDownList;
        public static void InitOptionDropDownList()
        {
            _optionDropDownList = new ValueDropdownList<int>();
            for (int i = 0; i < ConfigManager.instance.optionConfig.itemList.Count; i++)
            {
                var item = ConfigManager.instance.optionConfig.itemList[i];
                string question = item.question.IsFilled() ? item.question.Length > 10 ? item.question.Substring(0, 10) + "..." : item.question : "Empty";
                _optionDropDownList.Add($"{i}:{question}", i);
            }
        }

        public IEnumerable<OptionItemConfig> ItemConfigs
        {
            get
            {
                foreach (var itemIndex in list)
                {
                    yield return ConfigManager.instance.optionConfig.itemList[itemIndex];
                }
            }
        }
    }

    [Serializable]
    public class OptionItemConfig
    {
        [VerticalGroup("选项类型")]
        [HideLabel]
        [TableColumnWidth(100, false)]
        public OptionType type = OptionType.Single;
        [TextArea]
        [VerticalGroup("问题")]
        [HideLabel]
        public string question;

        [VerticalGroup("答案")]
        [LabelText("答案1")]
        public string answer1;

        [VerticalGroup("答案")]
        [LabelText("答案2")]
        [ShowIf("type", OptionType.Double)]
        public string answer2;

        [VerticalGroup("点击次数")]
        [TableColumnWidth(60, false)]
        [HideLabel]
        [MinValue(1)]
        public int clickCount = 5;

        [VerticalGroup("增加分数")]
        [TableColumnWidth(60, false)]
        [HideLabel]
        public int addScore = 5;

        [VerticalGroup("生命时长(秒)")]
        [TableColumnWidth(80, false)]
        [MinValue(0)]
        [HideLabel]
        public float lifeTime = 5;

        [VerticalGroup("飞行速度")]
        [TableColumnWidth(60, false)]
        [HideLabel]
        public float flySpeed = 1;

        [VerticalGroup("飞行模式")]
        [TableColumnWidth(60, false)]
        [HideLabel]
        public OptionFlyType flyType = OptionFlyType.PerlinNoise;
    }

    [Serializable]
    public class OptionCommonConfig
    {
        [TabGroup("飞行参数", "随机")]
        [LabelText("柏林抖动频率")]
        public float perlinShakeHz = 5;

        [TabGroup("飞行参数", "随机")]
        [LabelText("回心力的开始位置(位置到中心的距离与范围半径的比例)")]
        [PropertyRange(0, 1)]
        public float perlinCenterGravityStartPercent = 0.5f;

        [TabGroup("飞行参数", "随机")]
        [LabelText("最大回心力")]
        public float perlinCenterGravityMaxValue = 5;
    }

    public enum OptionType
    {
        [InspectorName("单个选项")]
        Single,
        [InspectorName("两个选项")]
        Double,
    }

    public enum OptionBelongStageType
    {
        [InspectorName("单个阶段")]
        Single,
        [InspectorName("多个阶段")]
        Range,
    }

    public enum OptionFlyType
    {
        [InspectorName("随机")]
        PerlinNoise,
    }
}

