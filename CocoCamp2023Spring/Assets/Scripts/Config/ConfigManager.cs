using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    public class ConfigManager : SingletonMonoBehaviourClass<ConfigManager>
    {
        public GameConfig gameConfig;
        public LevelConfig levelConfig;
        public OptionConfig optionConfig;
        public PlayerConfig playerConfig;
        public PlayAreaConfig playAreaConfig;
    }
}
