using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "PlayAreaConfig", menuName = "CreateConfig/PlayAreaConfig")]
    [Serializable]
    public class PlayAreaConfig : ScriptableObject
    {
        [LabelText("大脑动画配置")]
        [TableList(ShowIndexLabels = true)]
        public List<PlayAreaAnimConfig> animConfigs;
    }

    [Serializable]
    public class PlayAreaAnimConfig
    {
        [VerticalGroup("生成新选项中心点")]
        [HideLabel]
        public Vector2 center = Vector2.zero;

        [VerticalGroup("生成新选项半径")]
        [HideLabel]
        [MinValue(0)]
        public float radius = 5;

        [VerticalGroup("当前大脑放大倍数")]
        [HideLabel]
        [MinValue(0)]
        public float currentMaxScale = 5;

        [VerticalGroup("放大所需时间")]
        [HideLabel]
        [MinValue(0)]
        public float currentScaleTime = 1;

        [VerticalGroup("下一阶段开始出现时间")]
        [HideLabel]
        [MinValue(0)]
        public float nextFadeInStartTime = 1;

        [VerticalGroup("下一阶段出现所需时间")]
        [HideLabel]
        [MinValue(0)]
        public float nextFadeInNeedTime = 1;

    }
}

