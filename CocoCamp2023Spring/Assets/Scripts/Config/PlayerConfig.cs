using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "PlayerConfig", menuName = "CreateConfig/PlayerConfig")]
    [Serializable]
    public class PlayerConfig : ScriptableObject
    {
        [LabelText("最大加速度(惯性为0)")]
        public float maxAcc;
        [LabelText("最小加速度(惯性为1)")]
        public float minAcc;

        [LabelText("最大速度")]
        public float maxVel;

        //暂时是线性变化
        public float GetAccByInertia(float inertia)
        {
            return Mathf.Lerp(maxAcc, minAcc, inertia);
        }
    }
}

