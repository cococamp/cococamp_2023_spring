using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "LevelConfig", menuName = "CreateConfig/LevelConfig")]
    [Serializable]
    public class LevelConfig : ScriptableObject
    {
        [Header("所有关卡")]
        public LevelConfigItem[] levelList;
    }

    [Serializable]
    public class LevelConfigItem
    {
        public string SceneID;
    }
}

